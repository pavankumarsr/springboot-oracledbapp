package com.springBoot.oracleDbDemo;

import com.springBoot.oracleDbDemo.model.Employee;
import com.springBoot.oracleDbDemo.repository.EmployeeRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;


@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class EmployeeRepositoryTests {

    @Autowired
    private EmployeeRepository employeeRepository;

    //Junit
    @Test
    @Order(1)
    public void getEmployeeByEmployeeIdTest(){
        Employee employee=employeeRepository.findById(100L).get();
        Assertions.assertThat(employee.getEmployeeId()).isEqualTo(100L);
    }


    @Test
    @Order(2)
    public void getAllEmployeeTest(){
        List<Employee> employees=employeeRepository.findAll();
        Assertions.assertThat(employees.size()).isGreaterThan(0);
    }
}
