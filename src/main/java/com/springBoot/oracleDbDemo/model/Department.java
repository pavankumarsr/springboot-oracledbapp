package com.springBoot.oracleDbDemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity// this annotation specify that the class is an Entity
@Table(name="DEPARTMENTS") //this annotation specifies the table in the
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder//Junit
public class Department {

    @Id//this annotation specify that the primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY)//this annotation specifies the
    //generation strategies for the
    //values of primary key
    @Column(name="DEPARTMENT_ID")
    private long departmentId;
    @Column(name="DEPARTMENT_NAME")
    private String DepartmentName;
}
