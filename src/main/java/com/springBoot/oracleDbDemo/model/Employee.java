package com.springBoot.oracleDbDemo.model;

import javax.persistence.*;


import lombok.*;

@Entity// this annotation specify that the class is an Entity
@Table(name="EMPLOYEES") //this annotation specifies the table in the
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder//Junit
public class Employee {
    @Id//this annotation specify that the primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY)//this annotation specifies the
    //generation strategies for the
    //values of primary key
    @Column(name="EMPLOYEE_ID")
    private long employeeId;
    @Column(name="FIRST_NAME")
    private String firstName;
    @Column(name="LAST_NAME", nullable=false)
    private String lastName;
    @Column(name="EMAIL", nullable=false)
    private String email;

    @Column(name="SALARY")
    private String salary;

    @ManyToOne
    @JoinColumn(name="DEPARTMENT_ID")
    private Department department;




}
