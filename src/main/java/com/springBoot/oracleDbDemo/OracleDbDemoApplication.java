package com.springBoot.oracleDbDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication()
public class OracleDbDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OracleDbDemoApplication.class, args);
	}

}
