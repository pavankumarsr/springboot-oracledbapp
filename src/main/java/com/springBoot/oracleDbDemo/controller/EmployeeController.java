package com.springBoot.oracleDbDemo.controller;

import com.springBoot.oracleDbDemo.model.Employee;
import com.springBoot.oracleDbDemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
//http://localhost:8080/api/
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;


    public EmployeeController(EmployeeService employeeService) {
        super();
        this.employeeService = employeeService;
    }

    /**
     * getEmployees()
     * @return List<Employee>
     */
    //URL: http://localhost:8080/api/employees
    @GetMapping(path="/employees")
    public List<Employee> getEmployees(){
        return employeeService.getEmployees();

    }


}
