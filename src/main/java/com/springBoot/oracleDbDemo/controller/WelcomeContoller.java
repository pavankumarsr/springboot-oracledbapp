package com.springBoot.oracleDbDemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeContoller {
  @RequestMapping("/welcome")
    public String welcome(){
        return "welcome to springBoot!";
    }
}
