package com.springBoot.oracleDbDemo.repository;

import com.springBoot.oracleDbDemo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
