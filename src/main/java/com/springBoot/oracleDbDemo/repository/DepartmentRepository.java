package com.springBoot.oracleDbDemo.repository;

import com.springBoot.oracleDbDemo.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
