package com.springBoot.oracleDbDemo.service;

import java.util.List;
import com.springBoot.oracleDbDemo.model.Employee;
public interface EmployeeService {

    public List<Employee> getEmployees();
}
