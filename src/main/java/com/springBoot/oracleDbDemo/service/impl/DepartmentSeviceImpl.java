package com.springBoot.oracleDbDemo.service.impl;

import com.springBoot.oracleDbDemo.model.Department;
import com.springBoot.oracleDbDemo.repository.DepartmentRepository;
import com.springBoot.oracleDbDemo.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DepartmentSeviceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public DepartmentSeviceImpl(DepartmentRepository departmentRepository) {
        super();
        this.departmentRepository = departmentRepository;
    }


    @Override
    public List<Department> getDepartments() {
        return departmentRepository.findAll();
    }
}
