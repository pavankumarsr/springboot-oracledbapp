package com.springBoot.oracleDbDemo.service.impl;

import com.springBoot.oracleDbDemo.model.Employee;
import com.springBoot.oracleDbDemo.repository.EmployeeRepository;
import com.springBoot.oracleDbDemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        super();
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }


}
