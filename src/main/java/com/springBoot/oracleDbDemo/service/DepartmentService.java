package com.springBoot.oracleDbDemo.service;

import com.springBoot.oracleDbDemo.model.Department;

import java.util.List;

public interface DepartmentService {
    public List<Department> getDepartments();
}
